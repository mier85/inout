from django.shortcuts import render, redirect
from django.views.decorators.http import require_http_methods
from django.views.generic import TemplateView
from .forms import LoginForm
from django.contrib.auth import authenticate, login, logout


# Create your views here.
class Login(TemplateView):

    def get(self, request, *args, **kwargs):
        return render(request, 'login.html')

    def post(self, request, *args, **kwargs):
        form = LoginForm(request.POST)
        if form.is_valid:
            user = authenticate(username=request.POST.get('name'), password=request.POST.get('password'))
            if user is not None and user.is_active:
                login(request, user)
                if 'next' in request.GET:
                    return redirect(request.GET.get('next'))
                else:
                    return redirect('/')

        return render(request, 'login.html')


class Logout(TemplateView):
    def get(self, request, *args, **kwargs):
        logout(request)
        return redirect('/login/')