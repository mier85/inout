from django.conf.urls import *

from django.contrib import admin
from account.views import Login, Logout
from main.views import Overview, ItemUpdate, ItemCreate, ItemDelete, ItemDeleteConfirm
from django.contrib.auth.decorators import login_required, permission_required

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'inout.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^$', login_required(Overview.as_view()), name='overview'),

    url(r'^login/$', Login.as_view(), name="login"),
    url(r'^logout/$', Logout.as_view(), name="logout"),
    url(r'^admin/', include(admin.site.urls)),

    url(r'^item/create/$', login_required(ItemCreate.as_view()), name="item_create"),
    url(r'^item/update/(?P<id>\d+)/$', login_required(ItemUpdate.as_view()), name="item_update"),
    url(r'^item/delete/(?P<id>\d+)/$', login_required(ItemDelete.as_view()), name="item_delete"),
    url(r'^item/deleteConfirm/(?P<id>\d+)/$', login_required(ItemDeleteConfirm.as_view()), name="item_delete_modal")

)