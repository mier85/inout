from django.db import models
from django.contrib.auth.models import User


class Item(models.Model):
    user = models.ForeignKey(User)
    desc = models.CharField(max_length=100)
    date = models.DateField()
    amount = models.DecimalField(max_digits=19, decimal_places=2)
    expensed = models.BooleanField(default=True)

