from django.shortcuts import render, redirect
from django.views.decorators.http import require_http_methods
from django.contrib.auth.decorators import login_required
from django.views.generic import TemplateView
from main.models import Item
from main.forms import ItemForm
from datetime import datetime


def render_items(request):
        items = Item.objects.all().order_by('-date')
        item_form = ItemForm(initial={'date':datetime.now()})

        total = 0
        total_per_line = {}
        for item in reversed(items):
            total += item.amount
            total_per_line[item.id] = total

        return render(request, 'overview.html', {
            'items': items,
            'form': item_form,
            'total': total,
            'total_per_line': total_per_line
        })

# Create your views here.
class Overview(TemplateView):
    def get(self, request, *args, **kwargs):
        return render_items(request)

# Create your views here.
class ItemCreate(TemplateView):
    def post(self, request, *args, **kwargs):
        item_instance = Item()
        form = ItemForm(request.POST, instance=item_instance)
        if form.is_valid():
            item_instance = form.save(commit=False)
            item_instance.user = request.user
            item_instance.save()
            return redirect('overview')

        return render_items(request)


# Create your views here.
class ItemUpdate(TemplateView):
    def get(self, request, *args, **kwargs):
        item_instance = Item.objects.get(id=kwargs['id'])
        form = ItemForm(instance=item_instance)
        return render(request, 'update.html', {'form':form})


# Create your views here.
class ItemDelete(TemplateView):

    def get(self, request, *args, **kwargs):
        item = Item.objects.get(id=kwargs['id'])
        item.delete()
        return redirect('overview')


class ItemDeleteConfirm(TemplateView):
    def get(self, request, *args, **kwargs):
        item = Item.objects.get(id=kwargs['id'])
        return render(request, 'confirm.html', {'item': item})
