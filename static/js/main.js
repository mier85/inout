$(window).load(function(){
    $('.glyphicon-calendar').click(function(){
        var neighbour = $(this).parents('div').children('input');
        if ((typeof neighbour !== 'undefined') && (typeof neighbour.attr('type') !== 'undefined') && neighbour.attr('type') === 'date') {
            $(neighbour).trigger('focus');
        }
    });

$("th.debit").click(function(event){var cur=$('#id_amount').val(); $('#id_amount').val(cur >= 0 ? cur * -1 : cur);});

$("th.credit").click(function(event){var cur=$('#id_amount').val(); $('#id_amount').val(cur < 0 ? cur * -1 : cur);});
});
