$(window).load(function(){
    $('.deleteItem').click(function(event){
        url = $(event.currentTarget).attr("href");
        $.ajax({
            url:url,
            success:function(data){
                $("#deleteModal").find('.modal-content').html(data);
                $("#deleteModal").modal();
            }
        })
        event.preventDefault();
    });
})
